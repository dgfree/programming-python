from tkinter import *
from tkinter.messagebox import showinfo 

def reply(name):
    showinfo(title='reply', message='Hello %s' % name)

top = Tk()
# Title of window
top.title('Echo')
# top.iconbitmap('py-blue-trans-out.ico')

# Label of menu
Label(top, text="Enter your name:").pack(side=TOP)
# accepting user input
ent = Entry(top)
# put it at the top
ent.pack(side=TOP)
# put the submit button, use lambda so that the command is 
# done not when the button is created but when the button is pushed.
btn = Button(top, text="Submit", command=(lambda: reply(ent.get())))
btn.pack(side=LEFT)

top.mainloop()
