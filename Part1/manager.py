from person import Person

# a Manager class that inherits from the Person class in person.py
class Manager(Person):
    # override the __init__ method to give the job title as 'manager'
    def __init__(self, name, age, pay):
        Person.__init__(self, name, age, pay, 'manager')    

    # override the give_raise method to give a bonus to managers.
    def give_raise(self, percent, bonus=0.1):
        Person.give_raise(self, percent + bonus)

if __name__ == '__main__':
    # demo of the class
    tom = Manager(name = 'Tom Doe', age=50, pay=50000)
    print(tom.last_name())
    tom.give_raise(.20)
    print(tom.pay)
