from tkinter import *
from tkinter.messagebox import showinfo

# same as tkinter101.py, but with OOP
class MyGui(Frame):
    def __init__(self, parent=None):
        Frame.__init__(self, parent)
        button = Button(self, text='DO NOT PRESS', command=self.reply)
        button.pack()

    def reply(self):
        showinfo(title='Daniel\'s popup', 
                 message='I SAID DO NOT PRESS THE BUTTON!')

if __name__ == '__main__':
    window = MyGui()
    window.pack()
    window.mainloop()
