class Person:
    # initialize data for the class    
    def __init__(self, name, age, pay=0, job=None):
        self.name = name
        self.age = age
        self.pay = pay
        self.job = job

    # a way to print the class. Will let me print(object), ie print(tom)
    # and will return <tom => Person>. 
    # self.__class__ returns the lowest class, that way classes that inherit
    # from Person will return their own class name
    # ie, a Manager object that inherits from Person will return
    # <tom => Manager> instead of <tom => Person>
    def __str__(self):
        return '<%s => %s>' % (self.__class__.__name__, self.name)

    # returns the last name of the Person object.
    def last_name(self):
        return self.name.split()[-1]

    # increases pay by percent.
    def give_raise(self, percent):
        self.pay *= (1.0 + percent)

if __name__ == '__main__':
    bob = Person('Bob Smith', 42, 30000, 'software')
    sue = Person('Sue Jones', 45, 40000, 'hardware')
    print(bob.name, sue.pay)
    
    print(bob.last_name())
    sue.give_raise(.10)
    print(sue.pay)
