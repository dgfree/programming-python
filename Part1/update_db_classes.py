import shelve

db = shelve.open('class-shelve')

# get sue's data
sue = db['sue']
# give sue a raise
sue.give_raise(.25)
# update the database
db['sue'] = sue

# get tom's data
tom = db['tom']
# give tom a raise
tom.give_raise(.20)
# update the database
db['tom'] = tom
